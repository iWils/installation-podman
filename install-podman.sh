#!/bin/bash

echo -e "######   Installation Dépendance    ######"

yum install -y \
  curl \
  wget \
  btrfs-progs-devel \
  conmon \
  containernetworking-plugins \
  containers-common \
  crun \
  device-mapper-devel \
  git \
  glib2-devel \
  glibc-devel \
  glibc-static \
  go \
  golang-github-cpuguy83-md2man \
  gpgme-devel \
  iptables \
  libassuan-devel \
  libgpg-error-devel \
  libseccomp-devel \
  libselinux-devel \
  make \
  gcc \
  cmake \
  uidmap \
  pkgconfig

echo -e "######       Installation GO        ######"

export GOPATH=~/go
git clone https://go.googlesource.com/go $GOPATH
cd $GOPATH
cd src
./all.bash
export PATH=$GOPATH/bin:$PATH

echo -e "######     Installation CONMON      ######"

git clone https://github.com/containers/conmon
cd conmon
export GOCACHE="$(mktemp -d)"
make
make podman

echo -e "######      Installation RUNC       ######"

git clone https://github.com/opencontainers/runc.git $GOPATH/src/github.com/opencontainers/runc
cd $GOPATH/src/github.com/opencontainers/runc
make BUILDTAGS="selinux seccomp"
cp runc /usr/bin/runc

echo -e "######    Ajout de configuration    ######"

mkdir -p /etc/containers
curl -L -o /etc/containers/registries.conf https://src.fedoraproject.org/rpms/containers-common/raw/main/f/registries.conf
curl -L -o /etc/containers/policy.json https://src.fedoraproject.org/rpms/containers-common/raw/main/f/default-policy.json


echo -e "######     Installation Podman      ######"

TAG=$(curl -s https://api.github.com/repos/containers/podman/releases/latest|grep tag_name|cut -d '"' -f 4)
rm -rf podman*
wget https://github.com/containers/podman/archive/refs/tags/${TAG}.tar.gz
tar xvf ${TAG}.tar.gz
cd podman*/
make BUILDTAGS="selinux seccomp"
make install PREFIX=/usr
